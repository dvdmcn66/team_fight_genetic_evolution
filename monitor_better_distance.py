import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation

# Définir la taille de la fenêtre pour la moyenne glissante
window_size = 1000

def update_plot(i):
    # Lire le fichier CSV
    #df = pd.read_csv('distance.csv')
    file = 'log/distance__3_per_team_09:45.csv' #300
    df = pd.read_csv(file)

    filtered_data = df[df['distance'] < 10000]

    # Calculer la moyenne glissante
    rolling_average_nf = df['distance'].rolling(window_size).mean()
    rolling_average = filtered_data['distance'].rolling(window_size).mean()

    # Effacer le contenu du graphique
    plt.cla()

    # Tracer les données originales et la moyenne glissante
    plt.plot(rolling_average, label='Moyenne glissante')
    #plt.plot(rolling_average_nf, label='Valeur filtrée', marker='*', linestyle='None')
    plt.xlabel(file)
    plt.ylabel('Distance')
    plt.legend()

# Créer l'animation en rafraîchissant toutes les 1000 millisecondes (1 seconde)
ani = FuncAnimation(plt.gcf(), update_plot, interval=1000)

plt.show()
