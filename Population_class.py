import random

from conf import TOURNAMENT_SIZE
from Individual_class import Individual
import numpy as np

class Population:
    def __init__(self, color, nb_individuals):
        self.color = color
        self.nb_individuals = nb_individuals
        self.individuals = [Individual(color) for _ in range(nb_individuals)]

    def evaluate(self):
        for individual in self.individuals:
            individual.score = individual.score

    def select_parents(self):
        # Méthode de sélection des parents pour la reproduction
        # Par exemple, en utilisant la sélection par tournoi
        parents = []

        # Répéter la sélection jusqu'à ce que nous ayons suffisamment de parents
        while len(parents) < self.nb_individuals:
            tournament = random.sample(self.individuals, TOURNAMENT_SIZE)
            winner = max(tournament, key=lambda x: x.score)
            parents.append(winner)

        return parents

    def crossover(self, parent1, parent2):
        # Opération de croisement pour créer un nouvel individu
        # Par exemple, croisement à un point
        child = Individual(self.color)

        # Nous allons effectuer un croisement à un point.
        # Cela signifie que nous choisissons un point de coupure au hasard
        # et copions les poids du parent1 avant le point de coupure
        # et les poids du parent2 après le point de coupure.
        crossover_point = random.randint(0, child.nb_inputs)

        # Copier les poids du parent1 avant le point de coupure
        child.weights_input_hidden1[:crossover_point] = parent1.weights_input_hidden1[:crossover_point]
        child.weights_input_hidden2[:crossover_point] = parent1.weights_input_hidden2[:crossover_point]
        child.weights_hidden_output[:crossover_point] = parent1.weights_hidden_output[:crossover_point]

        # Copier les poids du parent2 après le point de coupure
        child.weights_input_hidden1[crossover_point:] = parent2.weights_input_hidden1[crossover_point:]
        child.weights_input_hidden2[crossover_point:] = parent2.weights_input_hidden2[crossover_point:]
        child.weights_hidden_output[crossover_point:] = parent2.weights_hidden_output[crossover_point:]

        return child

    def mutate(self, individual):
        # Opération de mutation pour modifier le réseau de neurones d'un individu
        # Par exemple, modification aléatoire des poids du réseau
        mutation_rate = 0.1

        # Nous allons parcourir les poids du réseau de neurones de l'individu
        # et avec une probabilité de `mutation_rate`, nous allons modifier chaque poids par une petite valeur aléatoire.
        for layer in [individual.weights_input_hidden1, individual.weights_input_hidden2, individual.weights_hidden_output]:
            for i in range(layer.shape[0]):
                for j in range(layer.shape[1]):
                    if np.random.rand() < mutation_rate:
                        # Ajouter une petite valeur aléatoire au poids
                        layer[i, j] += np.random.randn() * 0.1

    def evolve(self):
        self.evaluate()
        parents = self.select_parents()
        next_generation = []

        while len(next_generation) < self.nb_individuals:
            parent1, parent2 = random.sample(parents, 2)
            child = self.crossover(parent1, parent2)
            self.mutate(child)
            next_generation.append(child)

        self.individuals = next_generation
