import pygame

from Population_class import Population
from conf import BG_COLOR, REGEN_HEALTH, NB_PER_TEAM, \
    NB_FIGHT_PER_GEN, max_tour, NB_GEN, TKILL_PENALIZATION_HEALTH, TKILL_PENALIZATION_SCORE, \
    KILL_SCORE, DEATH_PEN, TEAM_SURVIVAL_SCORE, CAPTURE_THE_CENTER_SCORE, CAPTURE_THE_CENTER_PERSONAL_SCORE, \
    GOOD_DISTANCE_TO_CENTER_TO_SCORE, BAD_DISTANCE_TO_CENTER_TO_SCORE, FACTOR_EXAEQUO, screen, verbose, SCREEN_WIDTH, SCREEN_HEIGHT, \
    HIDDEN_SIZE, NB_HIDDEN_LAYER


# Initialisation de Pygame
pygame.init()



import datetime
time = datetime.datetime.now()
formatted_time = time.strftime("%H:%M:%S")
pygame.display.set_caption("FIGHT " + str(NB_PER_TEAM) + "V" + str(NB_PER_TEAM) + "_" + str(HIDDEN_SIZE) + "*" + str(NB_HIDDEN_LAYER) + "_" + formatted_time)

def check_collision():
    global blue_individual, green_individual, tkill, kill
    # Vérification des collisions

    for blue_individual in blue_individuals:
        if blue_individual.health > 0:
            for green_individual in green_individuals:
                if green_individual.health > 0:
                    if blue_individual.is_colliding_with(green_individual):
                        blue_individual.change_life(-1)
                        green_individual.change_life(-1)
                        if blue_individual.has_killed(green_individual):
                            green_individual.score -= DEATH_PEN
                            blue_individual.score += KILL_SCORE
                            kill += 1
                        if green_individual.has_killed(blue_individual):
                            blue_individual.score -= DEATH_PEN
                            green_individual.score += KILL_SCORE
                            kill += 1
    for individual1 in blue_individuals:
        if individual1.health > 0:
            for individual2 in blue_individuals:
                if individual2.health > 0 and individual1 != individual2:
                    if individual1.is_colliding_with(individual2):
                        individual1.change_life(TKILL_PENALIZATION_HEALTH)
                        individual2.change_life(TKILL_PENALIZATION_HEALTH)
                        if individual1.health <= 0:
                            individual2.score -= TKILL_PENALIZATION_SCORE
                            tkill += 1
                        if individual2.health <= 0:
                            individual1.score -= TKILL_PENALIZATION_SCORE
                            tkill += 1
    for individual1 in green_individuals:
        if individual1.health > 0:
            for individual2 in green_individuals:
                if individual2.health > 0 and individual1 != individual2:
                    if individual1.is_colliding_with(individual2):
                        individual1.change_life(TKILL_PENALIZATION_HEALTH)
                        individual2.change_life(TKILL_PENALIZATION_HEALTH)
                        if individual1.health <= 0:
                            individual2.score -= -TKILL_PENALIZATION_SCORE
                            tkill += 1
                        if individual2.health <= 0:
                            individual1.score -= -TKILL_PENALIZATION_SCORE
                            tkill += 1




def move_everybody():
    global individual
    # Déplacement des individus
    for individual in blue_individuals:
        if individual.health > 0:
            individual.move(individual.get_inputs(blue_individuals, green_individuals))
    for individual in green_individuals:
        if individual.health > 0:
            individual.move(individual.get_inputs(blue_individuals, green_individuals))


def regen_health():
    global green_individual, blue_individual
    for green_individual in green_individuals:
        if green_individual.health > 0:
            green_individual.change_life(REGEN_HEALTH)
    for blue_individual in blue_individuals:
        if blue_individual.health > 0:
            blue_individual.change_life(REGEN_HEALTH)


def check_lose(team):
    for individual in team:
        if individual.health > 0:
            return False
    return True

def count_alive(team):
    cpt = 0
    for individual in team:
        if individual.health > 0:
            cpt += 1
    return cpt

def distance(x1, x2, y1, y2):
    return ((x1-x2)**2+(y1-y2)**2)**0.5


def distance_team_to_center(team):
    # capture the center
    mean_distance = 0
    for individual in team:
        if individual.health > 0:
            mean_distance += distance(individual.x, SCREEN_WIDTH/2, individual.y, SCREEN_HEIGHT/2)
    nb_alive = count_alive(team)
    if nb_alive == 0:
        return 999999999
    else:
        return mean_distance / nb_alive

def determine_best_distance_to_center(population):
    better_distance = 10000000000
    for individual in population:
        d = distance(individual.x, SCREEN_WIDTH/2, individual.y, SCREEN_HEIGHT/2)
        if individual.health > 0 and d < better_distance:
            better_distance = d
    return better_distance

def compute_ratio_distance_score(d_to_center, max_score):
    return  max(0, min(((BAD_DISTANCE_TO_CENTER_TO_SCORE - d_to_center) / GOOD_DISTANCE_TO_CENTER_TO_SCORE),
                                           max_score))
def determine_winner_capture_the_center():
    global individual_winner

    with open('log/distance__' + str(NB_PER_TEAM) + '_per_team_' + formatted_time + "_" + str(HIDDEN_SIZE) + "*" + str(NB_HIDDEN_LAYER) + '.csv', 'a') as fichier:
        fichier.write(str(determine_best_distance_to_center(green_individuals + blue_individuals)) + "\n")
        fichier.write(str(determine_best_distance_to_center(green_individuals + blue_individuals)) + "\n")
    mean_distance_green = distance_team_to_center(green_individuals)
    mean_distance_blue = distance_team_to_center(blue_individuals)
    if mean_distance_blue < mean_distance_green:  # blues are occupying the center
        if verbose:
            print("Capture the center gagné par les bleux")
        better_distance_blue = determine_best_distance_to_center(blue_individuals)
        if better_distance_blue < BAD_DISTANCE_TO_CENTER_TO_SCORE:
            for individual_winner in blue_individuals:
                individual_winner.score +=  compute_ratio_distance_score(better_distance_blue, CAPTURE_THE_CENTER_SCORE)
    elif mean_distance_blue > mean_distance_green:  # greens are occupying the center
        if verbose:
            print("Capture the center gagné par les verts")
        better_distance_green = determine_best_distance_to_center(green_individuals)
        if better_distance_green < BAD_DISTANCE_TO_CENTER_TO_SCORE:
            for individual_winner in green_individuals:
                individual_winner.score += compute_ratio_distance_score(better_distance_green, CAPTURE_THE_CENTER_SCORE)
    else:
        if verbose:
            print("Capture the center ex-aequo")
        better_distance_green = determine_best_distance_to_center(green_individuals)
        if better_distance_green < BAD_DISTANCE_TO_CENTER_TO_SCORE:
            for individual_winner in green_individuals:
                individual_winner.score += compute_ratio_distance_score(better_distance_green, CAPTURE_THE_CENTER_SCORE) * FACTOR_EXAEQUO

        better_distance_blue = determine_best_distance_to_center(blue_individuals)
        if better_distance_blue < BAD_DISTANCE_TO_CENTER_TO_SCORE:
            for individual_winner in blue_individuals:
                individual_winner.score += compute_ratio_distance_score(better_distance_blue, CAPTURE_THE_CENTER_SCORE) * FACTOR_EXAEQUO

    for individual in green_individuals + blue_individuals:
        d_to_center = distance(individual.x, SCREEN_WIDTH/2, individual.y, SCREEN_HEIGHT/2)
        if individual.health > 0:
            individual.score += compute_ratio_distance_score(d_to_center, CAPTURE_THE_CENTER_PERSONAL_SCORE)


def manage_end_game(convergence):
    global individual_winner_blue, individual_winner_green

    if (check_lose(green_individuals) and check_lose(blue_individuals)) or \
            (count_alive(green_individuals) == count_alive(blue_individuals) and (convergence or tour > max_tour)):
        if verbose:
            print("ex-equo en kill             , bleux : ", count_alive(blue_individuals), " - verts : ",
              count_alive(green_individuals), \
              " --- tkill : ", tkill, " - kill : ", kill)
        for individual_winner_blue in blue_individuals + green_individuals:
            individual_winner_blue.score += TEAM_SURVIVAL_SCORE * FACTOR_EXAEQUO
        determine_winner_capture_the_center()
        return True
    elif check_lose(green_individuals) or \
            (count_alive(green_individuals) < count_alive(blue_individuals) and (convergence or tour > max_tour)):
        if verbose:
            print("Les bleus ont gagné en kill, bleux : ", count_alive(blue_individuals), " - verts : ",
              count_alive(green_individuals), \
              " --- tkill : ", tkill, " - kill : ", kill)
        for individual_winner_blue in blue_individuals:
            individual_winner_blue.score += TEAM_SURVIVAL_SCORE
        determine_winner_capture_the_center()
        return True
    elif check_lose(blue_individuals) or \
            (count_alive(green_individuals) > count_alive(blue_individuals) and (convergence or tour > max_tour)):
        for individual_winner_green in green_individuals:
            individual_winner_green.score += TEAM_SURVIVAL_SCORE
        if verbose:
            print("Les verts ont gagné en kill, bleux : ", count_alive(blue_individuals), " - verts : ",
              count_alive(green_individuals), \
              " --- tkill : ", tkill, " - kill : ", kill)
        determine_winner_capture_the_center()
        return True
    return False


# Création des individus

blue_population = Population("blue", NB_PER_TEAM * NB_FIGHT_PER_GEN)
green_population = Population("green", NB_PER_TEAM * NB_FIGHT_PER_GEN)


# Boucle principale du jeu
running = True
clock = pygame.time.Clock()

# Monitor distance
with open('log/distance__' + str(NB_PER_TEAM) + '_per_team_' + formatted_time + "_" + str(HIDDEN_SIZE) + "*" + str(NB_HIDDEN_LAYER) + '.csv', 'w') as fichier:
    fichier.write("distance\n"
                  )
for generation in range(NB_GEN):
    print("\ngeneration", generation)
    with open("SHOW_EVERY_X_GEN.txt", "r") as file:
        SHOW_EVERY_X_GEN = int(file.read())
    for i in range(NB_FIGHT_PER_GEN):
        break_instruction = False
        convergence = False
        blue_individuals = blue_population.individuals[i * NB_PER_TEAM:(i + 1) * NB_PER_TEAM]
        green_individuals = green_population.individuals[i * NB_PER_TEAM:(i + 1) * NB_PER_TEAM]
        tour = 0
        tkill = 0
        kill = 0
        prev_blue_states = [individual.get_state() for individual in blue_individuals]
        prev_green_states = [individual.get_state() for individual in green_individuals]
        while running:
            tour +=1
            if tour % 400 == 0:
                print("tour", tour)
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    running = False

            move_everybody()
            check_collision()
            regen_health()

            # Dessin de l'écran
            if generation % SHOW_EVERY_X_GEN == SHOW_EVERY_X_GEN-1 and i > 0 and i < 10:
                screen.fill(BG_COLOR)
                for individual in blue_individuals + green_individuals:
                    if individual.health > 0 :
                        individual.draw()
                    pygame.display.flip()
                # Limiter la vitesse du jeu à environ 60 images par seconde
                clock.tick(15)
            else:
                clock.tick(100000000)
            break_instruction = manage_end_game(convergence)
            if break_instruction:
                if verbose:
                    print("\ngeneration", generation)
                break

            if all(ind.get_state() == prev_blue_states[i] for i, ind in enumerate(blue_individuals)) and \
                    all(ind.get_state() == prev_green_states[i] for i, ind in enumerate(green_individuals)):
                if verbose:
                    print("Convergence")
                convergence = True

            prev_blue_states = [individual.get_state() for individual in blue_individuals]
            prev_green_states = [individual.get_state() for individual in green_individuals]



    blue_population.evolve()
    green_population.evolve()

    blue_individuals = blue_population.individuals
    green_individuals = green_population.individuals
# Quitter Pygame
pygame.quit()
