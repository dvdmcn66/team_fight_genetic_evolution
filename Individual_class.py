import random
import numpy as np
from conf import INDIVIDUAL_RADIUS, SPAWN_LIFE, NB_PER_TEAM, HIDDEN_SIZE, NB_HIDDEN_LAYER, INDIVIDUAL_SPEED, screen, \
    MAX_LIFE, SCREEN_WIDTH, SCREEN_HEIGHT
import pygame

class Individual:
    def __init__(self, color):
        self.color = color
        self.x = random.randint(INDIVIDUAL_RADIUS, SCREEN_WIDTH - INDIVIDUAL_RADIUS)
        self.y = random.randint(INDIVIDUAL_RADIUS, SCREEN_HEIGHT - INDIVIDUAL_RADIUS)
        self.health = SPAWN_LIFE
        self.score = 0

        # Initialisation du réseau de neurones avec des poids aléatoires
        self.nb_inputs = 3 * NB_PER_TEAM * 2 + 3
        self.hidden_size = HIDDEN_SIZE
        self.output_size = 2
        self.weights_input_hidden1 = np.random.randn(self.nb_inputs, self.hidden_size)
        self.weights_input_hidden2 = np.random.randn(self.hidden_size, self.hidden_size)
        self.weights_hidden_output = np.random.randn(self.hidden_size, self.output_size)

    def move(self, inputs):
        # Calcul du mouvement avec le réseau de neurones
        hidden_layer1 = np.dot(inputs, self.weights_input_hidden1)
        hidden_layer1 = np.tanh(hidden_layer1)

        if NB_HIDDEN_LAYER == 1:
            output_layer = np.dot(hidden_layer1, self.weights_hidden_output)
        elif NB_HIDDEN_LAYER == 2:
            # Calcul des activations pour la deuxième couche cachée
            hidden_layer2 = np.dot(hidden_layer1, self.weights_input_hidden2)
            hidden_layer2 = np.tanh(hidden_layer2)
            output_layer = np.dot(hidden_layer2, self.weights_hidden_output)

        #output_layer = np.tanh(output_layer)

        # Normalisation des valeurs de sortie entre -1 et 1
        #output_layer = np.clip(output_layer, -1, 1)

        output_layer = np.sign(output_layer)
        # Appliquer le mouvement basé sur les valeurs de sortie du réseau de neurones
        self.x += int(output_layer[0] * INDIVIDUAL_SPEED)
        self.y += int(output_layer[1] * INDIVIDUAL_SPEED)

        # Vérifier que l'individu ne sort pas de l'écran
        self.x = max(INDIVIDUAL_RADIUS, min(self.x, SCREEN_WIDTH - INDIVIDUAL_RADIUS))
        self.y = max(INDIVIDUAL_RADIUS, min(self.y, SCREEN_HEIGHT - INDIVIDUAL_RADIUS))

    def draw(self):
        pygame.draw.circle(screen, self.color, (self.x, self.y), INDIVIDUAL_RADIUS)

    def is_colliding_with(self, other_individual):
        distance_squared = (self.x - other_individual.x) ** 2 + (self.y - other_individual.y) ** 2
        return distance_squared <= (2 * INDIVIDUAL_RADIUS) ** 2
    def has_killed(self, other_individual):
        return other_individual.health <= 0

    def change_life(self, add):
        self.health = min(max(self.health+add, 0), MAX_LIFE)

    def get_inputs(self, blue_individuals, green_individuals):
        inputs = []
        for individual in blue_individuals:
            if individual != self and individual.health > 0:  # Ignorer les individus morts
                # Ajouter la position de chaque autre individu
                inputs.extend([individual.x - self.x, individual.y - self.y, individual.health])
            else:
                inputs.extend([-100000, -100000, -100000])
        for individual in green_individuals:
            if individual != self and individual.health > 0:  # Ignorer les individus morts
                # Ajouter la position de chaque autre individu
                inputs.extend([individual.x - self.x, individual.y - self.y, individual.health])
            else:
                inputs.extend([-100000, -100000, -100000])

        # Ajouter la position du joueur lui-même, relative au centre
        inputs.extend([SCREEN_WIDTH/2-self.x, SCREEN_HEIGHT/2-self.y])

        # Ajouter la vie du joueur
        inputs.append(self.health)

        return np.array(inputs)
    def get_state(self):
        # Retourne un tuple représentant l'état actuel de l'individu
        return (self.x, self.y, self.health, self.score)
