
# Définition des constantes
SCREEN_WIDTH, SCREEN_HEIGHT = 800, 600
BG_COLOR = (255, 255, 255)  # Couleur de l'arrière-plan (blanc)
BLUE_COLOR = (0, 0, 255)    # Couleur des individus bleus
GREEN_COLOR = (0, 255, 0)   # Couleur des individus verts
INDIVIDUAL_RADIUS = 50       # Rayon des individus
INDIVIDUAL_SPEED = 20        # Vitesse de déplacement des individus
MAX_LIFE = 5
SPAWN_LIFE = 2.5
REGEN_HEALTH = 0.2
NB_PER_TEAM = 2
POP_SELECTION = 200
NB_FIGHT_PER_GEN = int(POP_SELECTION/NB_PER_TEAM)

#genetic algorithm
TOURNAMENT_SIZE = 10

max_tour = 200
NB_GEN = 1000000
#SHOW_EVERY_X_GEN = 200
TKILL_PENALIZATION_HEALTH = 0
TKILL_PENALIZATION_SCORE = 0


#Score rules
KILL_SCORE = 0
DEATH_PEN = 0
TEAM_SURVIVAL_SCORE = 0
CAPTURE_THE_CENTER_SCORE = 2
CAPTURE_THE_CENTER_PERSONAL_SCORE = 2
GOOD_DISTANCE_TO_CENTER_TO_SCORE = 100
BAD_DISTANCE_TO_CENTER_TO_SCORE = 380
FACTOR_EXAEQUO = 0.3
# Création de la fenêtre
import pygame
screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))

#neural
HIDDEN_SIZE = 2
NB_HIDDEN_LAYER = 2 #1 or 2

#verbose
verbose = False


# Définition des constantes
SCREEN_WIDTH, SCREEN_HEIGHT = 800, 600
